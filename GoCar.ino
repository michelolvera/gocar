#include <AFMotor.h>
//Variables de motores
AF_DCMotor motorA(2);
AF_DCMotor motorB(4);
//Variables de ultrasonido
int Trig1 = A0;
int Echo1 = A1;
int Trig2 = A2;
int Echo2 = A3;
int Dist1;
int Dist2;
float out;
float x1, x2;
int dismaxcm = 20;
//Neurona
float K = 0.5;
float Pesos[2];
float bias = 0.5f;

void setup()
{
  //Usar pines analogicos como digitales
  pinMode(Trig1, OUTPUT);
  pinMode(Echo1, INPUT);
  pinMode(Trig2, OUTPUT);
  pinMode(Echo2, INPUT);
  //Inicializar motores
  motorA.setSpeed(50);
  motorB.setSpeed(50);
  motorA.run(RELEASE);
  motorB.run(RELEASE);
  // initialize serial:
  Serial.begin(9600);
  //Entrenamiento
  EntrenarNeurona(1, 1, 1);
  EntrenarNeurona(1, 0, 0.6666);
  EntrenarNeurona(0, 1, 0.3333);
  EntrenarNeurona(0, 0, 0);
}

void loop()
{
  //Serial.println("Entradas 1,1: ");
  //Serial.println(IniciarNeurona(0, 1));
  ultrasonido1(Dist1);
  ultrasonido2(Dist2);
  if(Dist1<dismaxcm){
      x1=1.0;
    }else if(Dist1<50){
      x1=float(100-(Dist1-dismaxcm)*100/30)/100;
      }else{
        x1=0.0;
        }
  if(Dist2<dismaxcm){
      x2=1.0;
    }else if(Dist2<50){
      x2=float(100-(Dist2-dismaxcm)*100/30)/100;
      }else{
        x2=0.0;
        }

  out = IniciarNeurona(x1,x2);
  Serial.print("Neurona dice: ");
  Serial.println(out);
  if (out < 0.15)//0,0
  {
    Serial.println("Deberia estar girando derecha we");
    girar_derecha();
    delay(500);
    Serial.println("despuesavanzar un poco we");
    avanzar();
    delay(500);
  }
  else if(out > 0.56 && out < 0.76)//1,0
  {
    Serial.println("deberia avanzar un poco we");
    avanzar();
  }
  else if(out > 0.23 && out < 0.43)//0,1
  {
    Serial.println("Deberia estar girando derecha we");
    girar_derecha();
    delay(500);
    Serial.println("despuesavanzar un poco we");
  }
  else if(out > 0.85)//1,1
  {
    Serial.println("Deberia estar girando a la izquierda we");
    girar_izquierda();
    delay(500);
  }
  EntrenarNeurona(1, 1, 1);
  EntrenarNeurona(1, 0, 0.6666);
  EntrenarNeurona(0, 1, 0.3333);
  EntrenarNeurona(0, 0, 0);
}

void girar_derecha()
{
  motorA.setSpeed(175);
  motorB.setSpeed(175);
  motorA.run(FORWARD);
  motorB.run(BACKWARD);
}

void girar_izquierda()
{
  motorA.setSpeed(175);
  motorB.setSpeed(175);
  motorA.run(BACKWARD);
  motorB.run(FORWARD);
}

void avanzar()
{
  motorA.setSpeed(100);
  motorB.setSpeed(100);
  motorA.run(BACKWARD);
  motorB.run(BACKWARD);
}

void detener()
{
  Serial.println("Paramesta");
  motorA.run(RELEASE);
  motorB.run(RELEASE);
}

void ultrasonido1(int &Distancia)
{
  //Para estabilizar el valor del pin Trig se establece a LOW
  digitalWrite(Trig1, LOW);
  delay(10);
  //Se lanzan los 8 pulsos
  digitalWrite(Trig1, HIGH);
  delay(10);
  digitalWrite(Trig1, LOW);
  /*
    Se mide el tiempo que tarda la señal en regresar y se calcula la distancia.

    Observa que al realizar pulseIn el valor que se obtiene es tiempo, no distancia

    Se está reutilizando la variable Distancia.
  */
  Distancia = pulseIn(Echo1, HIGH);
  Distancia = abs(Distancia / 58);
  delay(100);
}

void ultrasonido2(int &Distancia)
{
  //Para estabilizar el valor del pin Trig se establece a LOW
  digitalWrite(Trig2, LOW);
  delay(10);
  //Se lanzan los 8 pulsos
  digitalWrite(Trig2, HIGH);
  delay(10);
  digitalWrite(Trig2, LOW);
  /*
    Se mide el tiempo que tarda la señal en regresar y se calcula la distancia.

    Observa que al realizar pulseIn el valor que se obtiene es tiempo, no distancia

    Se está reutilizando la variable Distancia.
  */
  Distancia = pulseIn(Echo2, HIGH);
  Distancia = abs(Distancia / 58);
  delay(100);
}

float EntrenarNeurona(float x0, float x1, float target)
{
  float net = 0;
  float out = 0;
  float delta[2]; //Es la variacion de los pesos sinapticos
  float Error;

  net = Pesos[0] * x0 + Pesos[1] * x1 - bias;
  net = sigmoide(net);

  Error = target - net;

  bias -= K * Error; //Como el bias es siempre 1, pongo que
             //el bias incluye ya su peso sinaptico

  delta[0] = K * Error * x0; //la variacion de los pesos sinapticos corresponde
  delta[1] = K * Error * x1; //al error cometido, por la entrada correspondiente

  Pesos[0] += delta[0]; //Se ajustan los nuevos valores
  Pesos[1] += delta[1]; //de los pesos sinapticos

  out = net;
  return out;
}

float IniciarNeurona(float x0, float x1)
{
  float net = 0;
  float out = 0;

  net = Pesos[0] * x0 + Pesos[1] * x1 - bias;
  net = sigmoide(net);

  out = net;
  return out;
}

void pesos_IniciarNeurona(void)
{
  int i;
  for (i = 0; i < 2; i++)
  {
    Pesos[i] = (float)rand() / RAND_MAX;
  }
}

float sigmoide(float s)
{
  return (1 / (1 + exp(-1 * s)));
}
